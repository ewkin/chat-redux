const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './db.json';

let data = [];
module.exports = {
    init() {
        try {
            const fileContent = fs.readFileSync(filename);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    getItems() {
        return data.slice(Math.max(data.length - 30, 0));
    },
    addItem(item) {
        item.id = nanoid();
        item.datetime = new Date();
        data.push(item);
        this.save();
    },
    getItemByDate(date) {
        let index = data.findIndex(item => item.datetime === date);
        return [...data].splice(index + 1);
    },
    save() {
        fs.writeFileSync(filename, JSON.stringify(data, null, 2));
    }
};

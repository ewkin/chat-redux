const express = require('express');
const fileDb = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {
    if (typeof req.query.datetime === 'undefined') {
        const messages = fileDb.getItems();
        return res.send(messages);
    }
    const date = new Date(req.query.datetime);
    if (isNaN(date.getDate())) {
        return res.status(400).send({"error": 'Not a date'});
    } else {
        const message = fileDb.getItemByDate(date);
        res.send(message);
    }
});

router.post('/', (req, res) => {
    let message = req.body;
    if (message.author === '' || message.message === '') {
        return res.status(400).send({"error": 'Author and message must be present in the request'});
    }
    fileDb.addItem(req.body);
    res.send(req.body);
});

module.exports = router;
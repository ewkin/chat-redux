import {
    CHECK_MESSAGE,
    FETCH_CHAT_FAILURE,
    FETCH_CHAT_REQUEST,
    FETCH_CHAT_SUCCESS,
    SET_MESSAGE,
    SET_MESSAGES
} from "../actions/chatActions";

const initialState = {
    fetchLoading: false,
    fetchError: '',
    messages: [],
    datetime: '',
    checkMessage: false,
};

const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CHAT_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_CHAT_SUCCESS:
            return {...state, fetchLoading: false};
        case FETCH_CHAT_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.error};
        case SET_MESSAGES:
            return {
                ...state,
                messages: action.messages,
                datetime: action.messages[action.messages.length - 1].datetime
            };
        case SET_MESSAGE:

            return {
                ...state, checkMessage: false,
                messages: [...state.messages, action.message],
                datetime: action.message.datetime
            };
        case CHECK_MESSAGE:
            return {...state, checkMessage: true}
        default:
            return state;
    }
};

export default chatReducer;
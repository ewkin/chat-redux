import axiosChat from "../../axios-chat";

export const FETCH_CHAT_REQUEST = 'FETCH_CHAT_REQUEST';
export const FETCH_CHAT_SUCCESS = 'FETCH_CHAT_SUCCESS';
export const FETCH_CHAT_FAILURE = 'FETCH_CHAT_FAILURE';
export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_MESSAGE = 'SET_MESSAGE';
export const CHECK_MESSAGE = 'CHECK_MESSAGE';

export const fetchChatRequest = () => ({type: FETCH_CHAT_REQUEST});
export const fetchChatSuccess = () => ({type: FETCH_CHAT_SUCCESS});
export const fetchChatFailure = error => ({type: FETCH_CHAT_FAILURE, error});
export const setMessages = messages => ({type: SET_MESSAGES, messages});
export const setMessage = message => ({type: SET_MESSAGE, message});
export const checkMessage = () => ({type: CHECK_MESSAGE});


export const fetchMessages = () => {
    return async dispatch => {
        try {
            dispatch(fetchChatRequest());
            const response = await axiosChat.get('messages/');
            dispatch(setMessages(response.data));
            dispatch(fetchChatSuccess());
        } catch (error) {
            dispatch(fetchChatFailure(error));
        }
    }
};

export const fetchMessage = datetime => {
    return async dispatch => {
        try {
            dispatch(fetchChatRequest());
            const response = await axiosChat.get('messages?datetime=' + datetime);
            dispatch(setMessage(response.data[response.data.length-1]));
            dispatch(fetchChatSuccess());
        } catch (error) {
            console.log(error);
            dispatch(fetchChatFailure(error));
        }
    }
};

export const postMessage = message => {
    return async dispatch => {
        try {
            dispatch(fetchChatRequest());
            await axiosChat.post('messages/', message);
            dispatch(checkMessage());
            dispatch(fetchChatSuccess());
        } catch (error) {
            dispatch(fetchChatFailure(error));
        }
    };
};


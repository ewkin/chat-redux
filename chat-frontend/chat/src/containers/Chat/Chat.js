import React, {useEffect, useState} from 'react';
import moment from 'moment';
import InputText from "../../components/InputText/InputText";
import Messages from "../../components/Messages/Messages";
import {useDispatch, useSelector} from "react-redux";
import {fetchMessage, fetchMessages, postMessage} from "../../store/actions/chatActions";


const Chat = () => {

    const dispatch = useDispatch();

    const messages = useSelector(state => state.messages);
    const check = useSelector(state => state.checkMessage);
    const datetime = useSelector(state => state.datetime);

    const [myMessage, setMyMessage] = useState('');

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    useEffect(() => {

        if (check && messages.length>0) {
            console.log(datetime);
            dispatch(fetchMessage(datetime));
        }
    }, [check, datetime, messages.length,dispatch]);

    const handleChange = (event) => {
        setMyMessage(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        let message = {
            message: myMessage,
            author: "Kim"
        }
        dispatch(postMessage(message));
        setMyMessage('');

    };


    return (
        <div className="page-content page-container" id="page-content">
            <div className="padding">
                <div className="row container d-flex justify-content-center">
                    <div className="col-md-6">
                        <div className="card card-bordered">
                            <div className="card-header">
                                <h4 className="card-title"><strong>Chat</strong></h4>
                            </div>
                            <div className="ps-container">
                                <div id="chat">
                                    {messages.map(message => (
                                        <Messages
                                            key={message.id}
                                            author={message.author}
                                            message={message.message}
                                            date={moment(message.datetime).format('MMMM Do YYYY, h:mm:ss a')}
                                        />
                                    ))}
                                </div>
                                <InputText
                                    value={myMessage}
                                    handleChange={handleChange}
                                    handleSubmit={handleSubmit}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Chat;